var nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    }
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === 'staging') { //staging ---> servidor de pruebas
        console.log('XXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        }
        mailConfig = sgTransport(options);
    } else {
        //all mail are cached by ethereal.mail
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                //user: 'wellington.koepp@ethereal.email',
                user: process.env.ethereal_user,
                //pass: 'S3RfuhhgVpPqN3J1bR'
                pass: process.env.ethereal_pass
            }
        }
    }

    module.exports = nodemailer.createTransport(nodemailerConfig);